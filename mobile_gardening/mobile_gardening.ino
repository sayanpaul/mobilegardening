// #define BLYNK_PRINT Serial
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <ArduinoJson.h>
#include <stdio.h>
#define PUMP_1     0
#define ULTRASONIC_TX 2
#define ULTRASONIC_RX 3
#define APP_PUMP_BUTTON V1
#define APP_LED_IND V0
#define APP_TANK_WATER_LEVEL V2

#define PUMP_DURATION 7000
#define WATER_LEVEL_CHECK_INTERVAL 60000
#define TANK_LOWER_LEVEL 40
#define TANK_HIGH_LEVEL 10
#define RECONNECT_TIME 1000

//enter your credentials
char auth[] = "";
char ssid[] = "";
char pass[] = "";

unsigned long start_timer = 0;
unsigned long water_level_check_intervel = 0;
unsigned long blynk_status_duration = 0;
int pump_stat;
bool pump_run = false;
bool Connected2Blynk;
int tank_level= 0;

WiFiServer server(80);
String header;

BLYNK_WRITE(APP_PUMP_BUTTON){
pump_run = true;
}

BLYNK_READ(APP_TANK_WATER_LEVEL){
  Blynk.virtualWrite(APP_TANK_WATER_LEVEL,tank_level);
}

void setup()
{
  pinMode(ULTRASONIC_RX,INPUT);
  start_timer = 0;
  Serial.begin(9600);
  WiFi.begin(ssid, pass);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");  
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP()); 
  Blynk.config(auth);
  // Serial.println((Blynk.connect(9999));
  if (Blynk.connect(9999) == true) {
    Serial.println("Connected to Blynk server");
  }
  // timer.setInterval(11000L, CheckConnection);
  tank_level = WaterParcentage();
}

int WaterParcentage() {
  water_level_check_intervel =millis();
  digitalWrite(ULTRASONIC_TX, HIGH);
  delayMicroseconds(10);
  digitalWrite(ULTRASONIC_TX, LOW);
  float duration = (pulseIn(ULTRASONIC_RX, HIGH)); 
  int distance = int(duration/29.155/2);
  int range = (TANK_LOWER_LEVEL - TANK_HIGH_LEVEL);
  int percentage = (((range-distance)/range)*100);
  return percentage;
}

void StartPump() {
     if (pump_run && (start_timer ==0)) {
       Serial.println("inside pump START:");
      digitalWrite(PUMP_1 , HIGH);
      Blynk.virtualWrite(APP_LED_IND,1023);
      start_timer = millis();
    }
}

  void StopPump() {
    if ((millis()-start_timer >= PUMP_DURATION) && pump_run ) {
      Serial.println("inside pump STOP:");
      pump_run = false;
      digitalWrite(PUMP_1 , LOW);
      if(Connected2Blynk==1){
        Blynk.virtualWrite(APP_PUMP_BUTTON,LOW);
        Blynk.virtualWrite(APP_LED_IND,0);
      } 
      start_timer = 0;
    }
  }

void CheckConnection() {
  Connected2Blynk = Blynk.connected();
  if(Connected2Blynk== 0){
    Serial.println("Not connected to Blynk server"); 
    Blynk.connect(3333);  // timeout set to 10 seconds and then continue without Blynk  
  }
  else{
    blynk_status_duration = millis();
    Serial.println("Connected to Blynk server");     
  }
}

void SendResponse(WiFiClient client,int code,bool state ){
  StaticJsonBuffer<300> jsonBuffer;
  JsonObject& JSONencoder = jsonBuffer.createObject();
  char JSONmessageBuffer[300];
  String pump_state;
  char responsestatus[16];
  sprintf(responsestatus, "HTTP/1.1 %d OK",code);
  if (state) {
    pump_state = "Running";
  } else {
    pump_state = "Stopped";
  }
  JSONencoder["Pump"] = pump_state;
  JSONencoder["Tank"] = tank_level;
  JSONencoder.prettyPrintTo( JSONmessageBuffer, sizeof(JSONmessageBuffer ) );
  client.println(responsestatus);
  client.println("Content-type:Application/Json");
  client.println(JSONmessageBuffer);
  client.println();         
}

void loop() {
  Serial.println(Connected2Blynk);
  // check for connection after every 5 mins
  if (Connected2Blynk==0 && !pump_run &&((millis()- blynk_status_duration)>RECONNECT_TIME)){
    CheckConnection();
    Connected2Blynk = Blynk.connected();
    blynk_status_duration = millis();
  }

  // water level check after fixed interval
  if (!pump_run && (millis() - water_level_check_intervel > WATER_LEVEL_CHECK_INTERVAL)) {
    tank_level = WaterParcentage();
  }

  WiFiClient client = server.available();

  if(client &&  !pump_run){
    Serial.println("New Client.");
    String currentLine = "";
     while (client.connected()) {
       if (client.available()) {
         char c = client.read();
         Serial.write(c);
         header += c;
        if (currentLine.length() == 0) {
          if (header.indexOf("POST /pump/on") >= 0){
            if(pump_run){
              SendResponse(client,204,pump_run);
            } else{
              pump_run = true;
              start_timer = 0;
              SendResponse(client,200,pump_run);
              StartPump();
            }
          }
          if (header.indexOf("GET /status") >= 0){ 
            SendResponse(client,200,pump_run);
          }
        }
       }
     }

    
    }

  if(Connected2Blynk){  
    Blynk.run();
    pump_stat = digitalRead(PUMP_1);
    // if (pump_stat == 1){
    // Blynk.virtualWrite(APP_LED_IND,1023);
    // } else {
    //   Blynk.virtualWrite(APP_LED_IND,0);
    // }
   
  }

  StartPump();
  StopPump();

}
